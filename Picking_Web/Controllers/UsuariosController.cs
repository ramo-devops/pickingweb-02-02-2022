﻿using Picking_Web.Models;
using System.Web.Mvc;

namespace Picking_Web.Controllers
{
    public class UsuariosController : MyController
    {
        private ApplicationDbContext _context;

        public UsuariosController()
        {
            _context = new ApplicationDbContext();
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
        }

        // GET: Usuarios
        public ActionResult Index()
        {
            SetUserIdInViewBag();
            return View();
        }

        public ActionResult Novo()
        {
            return RedirectToAction("Register", "Account");
        }

        public ActionResult Editar(string id)
        {
            return RedirectToAction("Editar", "Account", new { id = id });
        }
    }
}