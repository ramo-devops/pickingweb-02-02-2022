﻿$(document).ready(function () {

    var table = $("#empresas").DataTable({
        ajax: {
            url: DefaultApiPath + "/getempresas",
            dataSrc: ""
        },
        columns: [
            {
                data: "nome",
                render: function (data, type, empresa) {
                    return "<a href='" + DefaultPath + "/editar/" + empresa.id + "'>" + data + "</a>";
                }
            },
            {
                data: "ativo",
                render: function (data, type, empresa) {
                    return data ? "Sim" : "Não";
                }
            },
            {
                data: "licenciado",
                render: function (data, type, empresa) {
                    return data ? "Sim" : "Não";
                }
            },
            {
                data: "instanciaBanco",
            },
            {
                data: "nomeBanco",
            },
            {
                data: "usuarioBanco",
            },
            {
                data: "id",
                render: function (data, type, empresa) {
                    return "<button data-empresa-id='" +
                        data +
                        "' type='button' class='btn btn-danger js-delete'><span class='glyphicon glyphicon-trash'></span >&nbsp;</button>";
                }
            }
        ],
        "oLanguage": _DEFAULT_SCRIPT_LANG
    });

    $("#empresas").on("click", ".js-delete", function () {

        var button = $(this);

        bootbox.confirm("Deseja deletar esta Empresa?", function (result) {

            if (result) {
                $.ajax({
                    url: DefaultApiPath + "/deletarempresas/" + button.attr("data-empresa-id"),
                    method: "DELETE",
                    success: function () {
                        toastr.success("empresa removida com sucesso.");
                        table.row(button.parents("tr")).remove().draw();
                    },
                    error: _DEFAULT_ERROR_TREATMENT
                });
            }
        });
    });
});