﻿$(document).ready(function () {

    var table = $("#usuarios").DataTable({
        ajax: {
            url: DefaultApiPath + "/getusuarios",
            dataSrc: ""
        },
        columns: [
            {
                data: "userName",
                render: function (data, type, usuario) {
                    return "<a href='" + DefaultPath + "/editar/" + usuario.id + "'>" + data + "</a>"
                }
            },
            {
                data: "email"
            },
            {
                data: "ativo",
                render: function (data, type, usuario) {
                    return data ? "Sim" : "Não";
                }
            },
            {
                data: "licenciado",
                render: function (data, type, usuario) {
                    return data ? "Sim" : "Não";
                }
            },
            {
                data: "operador",
                render: function (data, type, usuario) {
                    return data ? "Sim" : "Não";
                }
            },
            {
                data: "id",
                render: function (data, type, usuario) {
                    return "<button data-usuario-id='" +
                        data +
                        "' type='button' class='btn btn-danger js-delete'><span class='glyphicon glyphicon-trash'></span >&nbsp;</button>";
                }
            }
        ],
        "oLanguage": _DEFAULT_SCRIPT_LANG
    });

    $("#usuarios").on("click", ".js-delete", function () {

        var button = $(this);

        bootbox.confirm("Deseja deletar este usuário?", function (result) {

            if (result) {
                $.ajax({
                    url: DefaultApiPath + "/deletarusuario/" + button.attr("data-usuario-id"),
                    method: "DELETE",
                    success: function () {
                        toastr.success("Usuário removido com sucesso.");
                        table.row(button.parents("tr")).remove().draw();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        _DEFAULT_ERROR_TREATMENT(jqXHR, textStatus, errorThrown, "Erro ao deletar Usuário");
                    }
                });
            }
        });
    });
});